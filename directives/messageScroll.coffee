'use strict'

angular.module 'foxrey'
  .directive 'dialogueBody', (CONFIG, $window)->
    restrict: 'A'
    link: (scope, element, attrs, ctrl) ->
      isScrolled = false

      scroll = (a, height,element)->
        element.animate(
          {scrollTop: height},
          a
        )

      objectOp = ()->
        if $('html').width() > 882
          currentScroll = element.scrollTop()
          currentHeight = element[0].scrollHeight
          el = element
        else
          currentScroll = $('body').scrollTop()
          currentHeight = $('body').height()
          el = $('body')
        return {
          currentScroll: currentScroll
          el: el
          currentHeight: currentHeight
        }


      scope.$on CONFIG.EVENTS.inboxScrollBottom, ->
        object = objectOp()
        if !isScrolled
          console.log object
          scroll(800, object.currentHeight, object.el)
        else
          if object.el[0].scrollHeight - object.currentScroll < 700
            scroll(300, object.currentHeight, object.el)

        isScrolled = true

      scope.$on CONFIG.EVENTS.inboxScrollTop, ->
        object = objectOp()
        scroll(800, 0, object.el)

