'use strict'

angular.module 'foxrey'
  .directive 'hotkeySend', (CONFIG)->
    restrict: 'A'
    link: (scope, element, attrs, ctrl) ->
      form = element.parents('form')
      element.bind 'keydown', (e)->
        if e.ctrlKey && e.keyCode == 13
          form.triggerHandler('submit')




