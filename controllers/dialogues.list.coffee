'use strict'
angular.module "foxrey"
  .controller "InboxDialoguesCtrl", (GetTPDeliveries, Socket, $filter, $scope, $modal, Session, Inbox, isLoading) ->

    $scope.infiniteScrollControl =
      stopScroll: false

    skip = 0
    limit = 20

    #checking my messages
    $scope.isNotMyMessage = (message)->
      if message? && message.from_user_id
        return $filter('isNotMyMessage')(message, $scope.user.user)

    #ordering function
    $scope.orderChatrooms = ()->
      (item)->
        if $scope.isNotMyMessage(item.message) && !item.message.readed
          return false
        else
          return true

    #get chats func
    $scope.getChats = ()->
      if !isLoading.getLoading('getList')
        isLoading.setLoading('getList')
        $scope.infiniteScrollControl.stopScroll = true
        Inbox.get({skip: skip, limit: limit})
          .then (res) ->
            _.forEach res.data, (n)->
              index = _.findIndex $scope.data, (r)->
                r.message.id == n.message.id
              if index == -1
                $scope.data.push n
            if res.data.length != 0
              skip += res.data.length
              if res.data.length == limit
                $scope.infiniteScrollControl.stopScroll = false
            isLoading.removeLoading('getList')
          .catch (res) ->
            $scope.infiniteScrollControl.stopScroll = true
            isLoading.removeLoading('getList')

    init = ()->
      $scope.loading = isLoading

      $scope.data = []

      #get user
      Session.get().then (user)->
        $scope.user = user

      Socket.on 'unreaded_chat', (res) ->
        $scope.$apply ()->
          index = _.findIndex $scope.data, (n)->
            n.chat_room.id == res.chat_room.id
          if index != -1
            $scope.data[index] = res
          else
            $scope.data.push res

      $scope.getChats()

    init()

    #@TODO: modal window Compose: to be or not to be?
    #$scope.newMessage = ()->
    #  newMessageCtrl = ($scope, $modalInstance)->
    #    #GetTPDeliveries.get().then (res)->
    #    #    $scope.deliveries_id_list = res.data
#
    #    $scope.ok = ()->
    #      $modalInstance.close()
    #    $scope.cancel = ->
    #      $modalInstance.close()
#
    #  $modal.open
    #    templateUrl: '/app/inbox/views/partials/new_message.html'
    #    controller: newMessageCtrl
    #    size: 'sm'
    #    windowClass: 'modal-settings'
