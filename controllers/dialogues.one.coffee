'use strict'
angular.module "foxrey"
  .controller "InboxOneDialogueCtrl", ($scope, $timeout, $rootScope, $filter, $state, $stateParams, Inbox, isLoading, Session, gettext, FoxreyAlert, Socket, moment, CONFIG, $modal) ->

    #massive with dialogue show triggers
    $scope.showSub = {}

    $scope.loading = isLoading

    $scope.unread = []
    $scope.undelivered = []

    $scope.dateArr = []

    $scope.myMessages = []
    $scope.notMyMessages = []

    limit = 20

    $scope.allChatroomMessagesAreShown = false
    #trigger
    readedTrigger = false

    $scope.currentChatRoom = parseInt $stateParams.room_id

    #check several messages from one user
    $scope.isVisible = (prevMessage, message)->
      if prevMessage?
        isSameAuthor = prevMessage.from_user_id.id == message.from_user_id.id
        isShortInterval = moment(message.createdAt).minute() - moment(prevMessage.createdAt).minute() == 0
        return isSameAuthor && isShortInterval

    #not delivered mark
    $scope.isNotDelivered = (msg)->
      index = _.findIndex $scope.myMessages, (n)->
        msg.id == n.id
      if !_.isUndefined $scope.selfMember.user_id #undefined is not an object (evaluating '$scope.selfMember.user_id')
        return !$filter('isNotMyMessage')(msg, $scope.selfMember.user_id) && (index > ($scope.myMessages.length - 1 - $scope.notDeliveredCount)) && ($scope.notDeliveredCount != 0)

    #message actions menu
    $scope.showMessageMenu = (index, indexMessage)->
      _.forEach $scope.showSub, (n, key)->
        if index != key
          $scope.showSub[key] = false
        return
      $scope.showSub[index] = !$scope.showSub[index]
      if indexMessage = $scope.messages.length - 1
        $rootScope.$broadcast CONFIG.EVENTS.inboxScrollBottom

    #close all actions menu
    $scope.closeAllSub = ()->
      _.forEach $scope.showSub, (n, key)->
        $scope.showSub[key] = false
        return

    #send message form processer
    $scope.send = ()->
      if !isLoading.getLoading('sendingMessage')
        isLoading.setLoading('sendingMessage')
        message =
          chat_room_id: $scope.currentChatRoom
          message: $scope.newMessage

        Inbox.send(message).then (response)->
          isLoading.removeLoading('sendingMessage')
          $scope.chatMembers = response.data.allowed
          updateSelfMember()
          processed = newDataProcesser [response.data.message]
          $scope.messages.push processed[0]
          renewMarks()
          $scope.newMessage = ''
          $rootScope.$broadcast CONFIG.EVENTS.inboxScrollBottom

    #message readed request
    $scope.readed = ()->
      if !readedTrigger && $scope.selfMember.count > 0
        readedTrigger = true
        Inbox.read({chat_room_id: $scope.currentChatRoom})
        .then (response)->
          readedTrigger = false
          $scope.chatMembers = response.data
          updateSelfMember()
          $scope.unreadMark = false
        .catch (error)->
          readedTrigger = false

    #show previous messages request
    $scope.showPrev = ()->
      if !isLoading.getLoading('getMessages') && !$scope.allChatroomMessagesAreShown
        isLoading.setLoading('getMessages')
        Inbox.getOne($scope.currentChatRoom, {skip: $scope.messages.length, limit: limit})
        .then (roomInfo)->
          isLoading.removeLoading('getMessages')
          $scope.room_info = roomInfo.data.chat_room
          $scope.chatMembers = roomInfo.data.allowed
          updateSelfMember()
          newMessages = newDataProcesser roomInfo.data.messages
          if newMessages.length == 0 || newMessages.length < limit
            $scope.allChatroomMessagesAreShown = true
          $scope.messages.unshift newMessages...
          $rootScope.$broadcast CONFIG.EVENTS.inboxScrollTop
        .catch (error)->
          noChatRoomErrorProcesser error

    #leave chat
    $scope.leaveChatRoom = ()->
      if !isLoading.getLoading('leavingChat')
        isLoading.setLoading('leavingChat')
        Inbox.leave($scope.currentChatRoom)
        .then (response)->
          isLoading.removeLoading('leavingChat')
          $state.go('^.list', {}, {reload:true})
        .catch (e)->
          isLoading.removeLoading('leavingChat')

    #remove user
    $scope.removeUser = (id)->
      if !isLoading.getLoading('removingUser')
        isLoading.setLoading('removingUser')
        request =
          chat_room_id: $scope.currentChatRoom
          user_id: id
        Inbox.remove(request)
        .then (response)->
          isLoading.removeLoading('removingUser')
        .catch (e)->
          isLoading.removeLoading('removingUser')

    #delete message
    $scope.deleteMessage = (id)->
      if !isLoading.getLoading('deletingMessage')
        isLoading.setLoading('deletingMessage')
        Inbox.delete_message(id)
        .then (response)->
          isLoading.removeLoading('deletingMessage')
          deleteDataProcesser id
        .catch (e)->
          isLoading.removeLoading('deletingMessage')

    #open form
    $scope.edit = (message)->
      $scope.editingMessage = true
      $scope.targetEditing =
        message: message.message
        id: message.id

    #send edit request
    $scope.editRequest = ()->
      if !isLoading.getLoading('editingMessage')
        isLoading.setLoading('editingMessage')
        Inbox.edit($scope.targetEditing.id, {message: $scope.targetEditing.message})
        .then (res)->
          isLoading.removeLoading('editingMessage')
          $scope.editingMessage = false
          $scope.targetEditing = ''
          editDataProcesser res.data

        .catch (e)->
          isLoading.removeLoading('editingMessage')

    #open invite form
    $scope.calInviteForm = ()->

      inviteModalCtrl = ($scope, chat_room_id, $modalInstance) ->

        $scope.send = ->
          if !isLoading.getLoading 'invitingUser'
            isLoading.setLoading 'invitingUser'
            Inbox.invite({chat_room_id: chat_room_id, email: $scope.email})
            .then (r)->
              $scope.invitationSucceed = true
              isLoading.removeLoading 'invitingUser'
            .catch (e)->
              isLoading.removeLoading 'invitingUser'

        $scope.cancel = ->
          $modalInstance.close()


      modalEx = $modal.open
        templateUrl: '/app/inbox/views/partials/invite_modal.html'
        controller: inviteModalCtrl
        size: 'md'
        windowClass: 'modal-settings'
        type: 'info'
        resolve:
          chat_room_id: ()->
            $scope.currentChatRoom

    #deleting response processer
    deleteDataProcesser = (id)->
      messageIndex = _.findIndex $scope.messages, (char)->
        char.id == id
      $scope.messages.splice messageIndex, 1

    #editing response processer
    editDataProcesser = (res)->
      edited = _.find $scope.messages, (char)->
        char.id == res.id
      edited.edited = true
      edited.message = res.message
      edited.updatedAt = res.updatedAt

    #no such chatroom processer
    noChatRoomErrorProcesser = (error)->
      isLoading.removeLoading('getMessages')
      if error.data.code == 212018
        FoxreyAlert.show(
          type: 'error'
          title: gettext 'Error'
          text: gettext "This chatroom doesn't exist."
          ok: ->
            $state.go('^.list', {}, {reload:true})
        )

    newDataProcesser = (messages)->
      if messages.length == 0
        $scope.allChatroomMessagesAreShown = true
      else
        if messages.length < limit
          $scope.allChatroomMessagesAreShown = true
        today = moment().format('D MMMM YYYY')

        todayCondition = (n)->
          if moment(n.createdAt).format('D MMMM YYYY') == today
            n.dateMark = 'TODAY'
          else
            n.dateMark = true

        _.forEach messages, (n, key)->

          existingDate = _.find $scope.dateArr, (chr)->
            moment(chr.time).format('D MMMM YYYY') == moment(n.createdAt).format('D MMMM YYYY')

          if !existingDate
            todayCondition n
            $scope.dateArr.push
              id: n.id
              time: n.createdAt

          else
            if $scope.messages
              markedMessage = _.find $scope.messages, (chr)->
                chr.id == existingDate.id

              if markedMessage? && markedMessage.id > n.id
                markedMessage.dateMark = false
                existingDate.id = n.id
                todayCondition n

          access_level = _.find $scope.chatMembers, (chr)->
            chr.user_id.id == n.from_user_id.id

          if access_level
            n.from_user_id.access_level = access_level.access_level

          index = _.findIndex $scope.chatMembers, (char)->
            return char.user_id.id == n.from_user_id.id
          if index > -1
            n.chat_side = $scope.chatMembers[index].chat_side
        messageSeparator messages

      return messages

    #separate my messages from anothers
    messageSeparator = (msg)->
      _.forEach msg, (n, key)->
        if $filter('isNotMyMessage')(n, $scope.selfMember.user_id)
          $scope.notMyMessages.push n
        else
          $scope.myMessages.push n

    #marking function
    renewMarks = ()->
      #detect unread mark
      if $scope.selfMember.count > 0
        $scope.unreadMark = $scope.notMyMessages[$scope.notMyMessages.length - $scope.selfMember.count]

      #detect not delivered mark
      anothersCountArr = []
      _.forEach $scope.chatMembers, (n, key)->
        if n.user_id.id != $scope.selfMember.user_id.id
          anothersCountArr.push n.count
      $scope.notDeliveredCount = Math.min.apply(this, anothersCountArr)

    #update selfmember
    updateSelfMember = ()->
      $scope.selfMember = _.find $scope.chatMembers, (char)->
        return char.user_id.id == $scope.user.user.id

    init = ()->

      #get user
      Session.get().then (user)->
        $scope.user = user

      #get chatroom messages

      isLoading.setLoading('getMessages')
      Inbox.getOne($scope.currentChatRoom)
      .then (roomInfo)->
        isLoading.removeLoading('getMessages')
        $scope.room_info = roomInfo.data.chat_room
        $scope.chatMembers = roomInfo.data.allowed
        updateSelfMember()
        $scope.messages = newDataProcesser roomInfo.data.messages
        renewMarks()
        $timeout ()->
          $rootScope.$broadcast CONFIG.EVENTS.inboxScrollBottom
        , 300
      .catch (error)->
        noChatRoomErrorProcesser error

      Socket.on 'new_message', (res) ->
        if $scope.currentChatRoom == res.allowed[0].chat_room_id
          $scope.$apply ()->
            $scope.chatMembers = res.allowed
            updateSelfMember()
            if $scope.selfMember
              message = newDataProcesser [res.message]
              $scope.messages.push message[0]
              renewMarks()
              $rootScope.$broadcast CONFIG.EVENTS.inboxScrollBottom
            else
              $state.go('^.list', {}, {reload:true})

      Socket.on 'read_messages', (res) ->
        if $scope.currentChatRoom == res[0].chat_room_id
          $scope.$apply ()->
            $scope.notDelivered = null
            $scope.chatMembers = res
            updateSelfMember()
            renewMarks()
            $rootScope.$broadcast CONFIG.EVENTS.inboxScrollBottom

      Socket.on 'edit_message', (res) ->
        if $scope.currentChatRoom == res.chat_room_id
          $scope.$apply ()->
            editDataProcesser res

      Socket.on 'delete_message', (res) ->
        if $scope.currentChatRoom == res.message.chat_room_id
          $scope.$apply ()->
            deleteDataProcesser res.message.id

    init()

    $scope.viewUserInfo = (user, company, chat_side)->

      userInfoModalCtrl = ($scope, $modalInstance, user, company, removeUser, selfUser, chatMembers, chat_side) ->
        $scope.user = user
        $scope.company = company
        $scope.self = selfUser
        $scope.chatMembers = chatMembers

        $scope.canIRemove = () ->
          return (user.id != selfUser.user_id.id) && (((selfUser.chat_side == chat_side) && (selfUser.access_level == 'Superadmin') && (user.access_level != 'Superadmin')) || ((selfUser.chat_side == chat_side) && (selfUser.access_level == 'Admin') && (user.access_level == 'Invited')))

        $scope.removeUser = (id) ->
          removeUser id

        $scope.cancel = ->
          $modalInstance.close()

      $modal.open
        templateUrl: '/app/inbox/views/partials/view_user_info.html'
        controller: userInfoModalCtrl
        windowClass: 'modal-settings'
        type: 'info'
        resolve:
          user: () ->
            user
          company: () ->
            company
          chat_side: () ->
            chat_side
          selfUser: () ->
            $scope.selfMember
          chatMembers: () ->
            $scope.chatMembers
          removeUser: () ->
            $scope.removeUser
        ok: ->
          return 'OK'
        cancel: ->
          return 'Cancel'


    $scope.$on '$destroy', ()->
      if $scope.selfMember
        $scope.readed()