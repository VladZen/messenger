'use strict'

angular.module "foxrey"
  .filter 'isNotMyMessage', () ->
    #check my message
    (m, self)->
      m.from_user_id.id != self.id
