'use strict'

angular.module "foxrey"
  .factory 'NewMessage', ($modal) ->
    class Factory
      constructor: ->

      open: (delivery) ->

        newMessageCtrl = ($scope, $modalInstance, isLoading, delivery, Inbox, $state, ShipperDelivery)->
          $scope.bids = delivery.bids

          $scope.loading = isLoading
          if delivery.bids?
            $scope.newMessage = {}
            $scope.newMessage.bid_id = 'undefined'

          getOwnerFunc = ()->
            isLoading.setLoading('getOwner')
            ShipperDelivery.getOwner(delivery.delivery.id)
            .then (r)->
              $scope.owner = r.data
              isLoading.removeLoading('getOwner')
            .catch (e)->
              isLoading.removeLoading('getOwner')

          getOwnerFunc()

          $scope.ok = ()->
            if !isLoading.getLoading('sendingMessage')
              isLoading.setLoading('sendingMessage')
              request =
                delivery_id: delivery.delivery.id
                subject: $scope.newMessage.subject

              request.bid_id = parseInt $scope.newMessage.bid_id if $scope.newMessage.bid_id?

              Inbox.create(request).then (res)->
                message =
                  chat_room_id: res.data.id
                  message: $scope.newMessage.message

                Inbox.send(message).then (response)->
                  isLoading.removeLoading('sendingMessage')
                  $modalInstance.close()
          $scope.cancel = ->
            $modalInstance.close()

        $modal.open
          templateUrl: '/app/inbox/views/partials/new_message.html'
          controller: newMessageCtrl
          size: 'md'
          windowClass: 'modal-settings'
          resolve:
            delivery: ()->
              delivery

    new Factory()