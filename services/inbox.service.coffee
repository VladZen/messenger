'use strict'

angular.module "foxrey"
  .factory 'Inbox', (API, CONFIG) ->
    class Factory
      constructor: ->

      get: (obj)->
        API.get CONFIG.API_URLS.get_chat_rooms, obj, false

      getOne: (id, skip)->
        API.get CONFIG.API_URLS.get_chat_room + id, skip, false

      create: (request)->
        API.post CONFIG.API_URLS.create_chat_room, request

      send: (request)->
        API.post CONFIG.API_URLS.send_message, request

      edit: (id, request) ->
        API.put CONFIG.API_URLS.edit_message + id, request

      read: (request) ->
        API.put CONFIG.API_URLS.read_message, request

      invite: (request) ->
        API.post CONFIG.API_URLS.invite_chat_room, request

      remove: (request) ->
        API.delete CONFIG.API_URLS.remove_user, request

      leave: (id) ->
        API.delete CONFIG.API_URLS.leave_chat_room + id

      delete_message: (message_id) ->
        API.delete CONFIG.API_URLS.delete_message + message_id

    new Factory()